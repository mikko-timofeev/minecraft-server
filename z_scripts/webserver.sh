#!/bin/sh

cd /home/mikko/minecraft-server/

#last_http_server=$(ps -fA | grep 'python3 -m http.server' | awk '{print $3}' | head -n 1)
#kill -9 $last_http_server 2> /dev/null

python3 -m http.server 8080 \
  --directory ./public
