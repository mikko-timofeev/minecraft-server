worlds['Utopia'] = './Utopia'

renders['survivalday'] = {
    'world': 'Utopia',
    'title': 'World of Utopia',
    'rendermode': normal, # smooth_lighting,
    'dimension': 'overworld',
    'forcerender': True,
}

renders['survivalnether'] = {
    'world': 'Utopia',
    'title': 'Nether of Utopia',
    'rendermode': nether, # nether_lighting,
    'dimension': 'nether',
    'forcerender': True,
}

renders['survivalend'] = {
    'world': 'Utopia',
    'title': 'End of Utopia',
    'rendermode': normal, # end_lighting,
    'dimension': 'end',
    'forcerender': True,
}

outputdir = './public/map'

texturepath = '/home/mikko/.minecraft/versions/1.16.5/1.16.5.jar'

imgformat = 'webp'

defaultzoom = 2

northdirection = 'lower-right'
