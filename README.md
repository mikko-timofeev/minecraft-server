# Minecraft server (Linux)

This is a simple Fabric Minecraft server to start playing rightaway with minimal effort.
It is configured to work well with both: the standalone server and at self-hosted setup.

## Project structure

```
  minecraft-server
  ├── banned-ips.json
  ├── banned-players.json
  ├── config
  │   └── (output removed)
  ├── eula.txt
  ├── fabric-server-launcher.properties
  ├── fabric-server-launch.jar
  ├── log4j2.xml
  ├── mods
  │   └── (output removed)
  ├── ops.json
  ├── overviewer_config.py
  ├── public
  │   ├── favicon.png
  │   ├── index.html
  │   ├── logs
  │   │   └── (output removed)
  │   └── map
  │       └── (output removed)
  ├── README.md
  ├── server-icon.png
  ├── server.jar
  ├── server.properties
  ├── usercache.json
  ├── Utopia
  │   ├── advancements
  │   │   └── (output removed)
  │   ├── data
  │   │   └── (output removed)
  │   ├── datapacks
  │   │   └── (output removed)
  │   ├── DIM-1
  │   │   └── data
  │   ├── DIM1
  │   │   └── data
  │   ├── level.dat
  │   ├── level.dat_old
  │   ├── playerdata
  │   │   └── (output removed)
  │   ├── poi
  │   │   └── (output removed)
  │   ├── region
  │   │   └── (output removed)
  │   ├── session.lock
  │   └── stats
  │       └── (output removed)
  ├── whitelist.json
  ├── z_scripts
  │   ├── minecraftserver.sh
  │   ├── overviewer_map_make.sh
  │   ├── overviewer_setup
  │   │   ├── build.sh
  │   │   ├── Dockerfile
  │   │   └── textures_download.sh
  │   ├── port_80.sh
  │   ├── systemd_setup
  │   │   ├── minecraftserver_in.sh
  │   │   ├── minecraftserver_rm.sh
  │   │   ├── webserver_in.sh
  │   │   └── webserver_rm.sh
  │   └── webserver.sh
  └── z_services
      ├── minecraft.service
      └── web.service
```
(created with awesome `tree -L 3` command, some output was removed)

## Components' description

[Fabric](https://fabricmc.net/) - minimally modded Minecraft server.
[Overviewer](https://overviewer.org/) - simple yet feature-rich mapping tool (web browser view).
[http.server](https://docs.python.org/3/library/http.server.html) - tool to serve Overviewer map and webpage with server info.

## Usage

### Minecraft server manual start
```
  ./z_scripts/minecraftserver.sh
```

### Web server manual start (to port 8080)
```
  ./z_scripts/webserver.sh
```

### (Optional) Overviewer containered build
```
  ./z_scripts/overviewer_setup/build.sh
```

### (Optional) Overviewer textures download
```
  ./z_scripts/overviewer_setup/textures_download.sh
```
This is not needed if you have minecraft client installed.

### Create overview map of current server state
```
  ./z_scripts/overviewer_map_make.sh
```
You can install `overviewer.py` in the system or build it in a container.

### (Optional) Minecraft server autostart
Add:
```
  ./z_scripts/systemd_setup/minecraftserver_in.sh
```

Remove:
```
  ./z_scripts/systemd_setup/minecraftserver_rm.sh
```

### (Optional) Web server autostart
Add:
```
  ./z_scripts/systemd_setup/webserver_in.sh
```

Remove:
```
  ./z_scripts/systemd_setup/webserver_rm.sh
```

### (Optional) Route requests from port 80 to 8080
```
  ./z_scripts/port_80.sh
```
This allows opening webpage (and map) without specifying port `:8080`.

## Recommended mods

Some good ones are listed [here](.miscellaneous/recommended_mods.yml).

## Connection / hosting

For people with no public domain or static IP I recommend Zerotier:

1. [Register account](https://my.zerotier.com/)
2. Create a network (optional, not required if your friends have one)
3. Join network (requires `zerotier-one` package from [official download page](https://www.zerotier.com/download/) or from your [distro's repo](https://software.opensuse.org/package/zerotier-one?search_term=zerotier-one))
```
  sudo systemctl enable zerotier-one
  sudo systemctl start zerotier-one
  sudo zerotier-cli join XXXXXXXXXXXXXXXX
```
4. Find the servers IP
a) From web interface ("Members" section):
```
  https://my.zerotier.com/network/XXXXXXXXXXXXXXXX
```
By default new members (who requested access) need to be authorised (tick checkbox at "Auth?" column)
b) From `ip address` output from the server machine (interface name starts with "zt")


For the example of home server configuration, check [(tutorial) home server](https://gitlab.com/-/snippets/2201261).

---

## TODO

1. Add env (sample) file for better configuration management (instead of replacing values in scripts).
2. Figure out persistent way to bind port 80 (`iptables-persistent` maybe?).
3. Improve systemd services' files.
